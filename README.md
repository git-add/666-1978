# 666-1978

`3 books` `all files encrypted`

---

[Linear Algebra](./bok%253A978-1-4615-9995-1.zip)
<br>
Larry Smith in Undergraduate Texts in Mathematics (1978)

---

[Basic Concepts of Algebraic Topology](./bok%253A978-1-4684-9475-4.zip)
<br>
Fred H. Croom in Undergraduate Texts in Mathematics (1978)

---

[Introduction to College Mathematics with A Programming Language](./bok%253A978-1-4613-9422-8.zip)
<br>
Edward J. LeCuyer Jr. in Undergraduate Texts in Mathematics (1978)

---




